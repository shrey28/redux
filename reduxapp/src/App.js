import React from "react";
import "./App.css";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { incNumber, decNumber } from "./Actions/index";
import { mobile } from "./Responsive";
const Container = styled.div`
  display: block;
  justify-content: space-around;
  margin: 0 auto;
  width:60%;
  height: 80vh;

  ${mobile({ display: "grid", justifyContent: "space-around" })}
`;
const Title = styled.h1`
  display: block;
  text-align: center;
  font-size: 50px;

  color: #af5dea;
  :hover {
    color: #b5bcbb;
  }
  ${mobile({ display: "flex", justifyContent: "space-around" })}
`;
const Buttonbox = styled.div`
  display: flex;
  justify-content: space-evenly;

  ${mobile({ display: "grid", justifyContent: "space-around" })};
`;
const P = styled.p`
  display: flex;
  justify-content: center;
  font-size: 22px;
  opacity: 0.6;
  color: #7a21a0;
`;

const App = () => {
  const myState = useSelector((state) => state.changeTheNumber);
  const dispatch = useDispatch();
  return (
    <Container>
      <Title>Welcome to Redux</Title>
      <P>Using Reect and Redux</P>

      <Buttonbox>
        <Button variant="contained" onClick={() => dispatch(decNumber())}>
          Decrement
          <RemoveIcon />
        </Button>
        <Title>{myState}</Title>
        <Button variant="contained" onClick={() => dispatch(incNumber())}>
          Increment <AddIcon />
        </Button>
      </Buttonbox>
    </Container>
  );
};

export default App;
